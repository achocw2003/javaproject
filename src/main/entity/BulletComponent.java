package main.entity;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer; // Import Timer from javax.swing
import main.constants.Constants;
import main.entity.element.BulletElement;

public class BulletComponent extends Component implements ActionListener {

    private static final long serialVersionUID = 1L;
    private int bulletSize;
    private Timer timer;
    private BulletElement[] bulletElements;
    private int size; // Current number of elements in the array
    private static final int INITIAL_ARRAY_SIZE = 10; // Initial size of the array

    public BulletComponent(int bulletSize) {
        super();
        this.bulletSize = bulletSize;
        timer = new Timer(1000, this); // Create a Timer from javax.swing.Timer
        bulletElements = new BulletElement[INITIAL_ARRAY_SIZE];
        size = 0;
    }

    public void active() {
        timer.start(); // Use start() method from javax.swing.Timer
    }

    public int getBulletSize() {
        return bulletSize;
    }

    public void setBulletSize(int bulletSize) {
        this.bulletSize = bulletSize;
    }

    private void resizeArray() {
        int newSize = bulletElements.length * 2; // Double the size
        BulletElement[] newArray = new BulletElement[newSize];
        System.arraycopy(bulletElements, 0, newArray, 0, size);
        bulletElements = newArray;
    }

    private void move() {
        for (int i = 0; i < size; i++) {
            BulletElement bulletElement = bulletElements[i];
            if (bulletElement.getY() - Constants.STEP_DISTANCE >= Constants.WALL_SIZE) {
                bulletElement.setY(bulletElement.getY() - Constants.STEP_DISTANCE);
            } else {
                // Remove element if it's beyond the wall
                removeBulletElementAtIndex(i);
                i--; // Adjust the index after removal
            }
        }
    }

    // New method to remove a specific BulletElement at the given index
    public void removeBulletElement(BulletElement bulletElement) {
        int index = -1;

        // Find the index of the bulletElement in the array
        for (int i = 0; i < size; i++) {
            if (bulletElements[i] == bulletElement) {
                index = i;
                break;
            }
        }

        // If the element is found, remove it
        if (index != -1) {
            // destroyElement(index);
        }
    }

    private void destroyElement(int index) {
	}

	private void removeBulletElementAtIndex(int index) {
        // Move elements after the removed index
        System.arraycopy(bulletElements, index + 1, bulletElements, index, size - index - 1);
        size--;
    }

    public void destroy() {
        timer.stop();
        bulletElements = new BulletElement[INITIAL_ARRAY_SIZE];
        size = 0;
    }

    public void draw(Graphics g) {
        for (int i = 0; i < size; i++) {
            BulletElement bulletElement = bulletElements[i];
            bulletElement.draw(g, this.getParent());
        }
    }

    public void add() {
        if (size == bulletElements.length) {
            resizeArray();
        }

        BulletElement bulletElement = new BulletElement(this.getX(), this.getY(), bulletSize, bulletSize);
        bulletElements[size] = bulletElement;
        size++;
    }

    public BulletElement[] getBulletElements() {
        // Return a copy of the array to prevent external modifications
        BulletElement[] copy = new BulletElement[size];
        System.arraycopy(bulletElements, 0, copy, 0, size);
        return copy;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        move();
        this.getParent().repaint();
    }
}
