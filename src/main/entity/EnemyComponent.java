package main.entity;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

import javax.swing.Timer;

import main.constants.Constants;
import main.constants.UrlConstants;
import main.entity.element.BulletElement;
import main.entity.element.EnenyElement;
import main.utils.FileUtil;

public class EnemyComponent extends Component implements ActionListener {
    private static final long serialVersionUID = 1L;

    private static final int TIME_STEP_DOWN = 4;
    private EnenyElement[] enenyElements;
    private int uniqueEnemyElementSize;
    private Timer timer;
    private boolean leftDirection;
    private int distanceElement;
    private int distanceElementDown;
    private int componentSize;
    private int timeToDown = TIME_STEP_DOWN;
    private String[] enemyInput;
    private int enemyOrder;
    private boolean winStatus;
    private int score;

    public EnemyComponent(int uniqueEnemyElementSize) {
        super();
        this.uniqueEnemyElementSize = uniqueEnemyElementSize;
        initAttribute();
    }

    private void generateEnemyInput() {
        enemyInput = new String[]{
                UrlConstants.ENEMY_MATRIX_1,
                UrlConstants.ENEMY_MATRIX_2,
                UrlConstants.ENEMY_MATRIX_3,
                UrlConstants.ENEMY_MATRIX_4,
                UrlConstants.ENEMY_MATRIX_5
        };
    }

    private void initAttribute() {
        score = 0;
        timer = new Timer(Constants.ENEMY_TIME_DELAY, this);
        leftDirection = false;
        enemyOrder = 0;
        generateEnemyInput();
    }

    public EnenyElement[] getEnenyElements() {
        return enenyElements;
    }

    public void setEnenyElements(EnenyElement[] enenyElements) {
        this.enenyElements = enenyElements;
    }

    private void populateEnemyElements(String enemyFileUrl) {
        int[][] enemyMatrix = FileUtil.getIntMatrix(enemyFileUrl);
        enenyElements = new EnenyElement[enemyMatrix.length * enemyMatrix[0].length];
        int count = 0;

        for (int i = 0; i < enemyMatrix.length; i++) {
            for (int j = 0; j < enemyMatrix[i].length; j++) {
                if (Objects.equals(enemyMatrix[i][j], 1)) {
                    enenyElements[count] = new EnenyElement(
                            this.getX() + j * uniqueEnemyElementSize,
                            this.getY() + i * uniqueEnemyElementSize,
                            uniqueEnemyElementSize,
                            uniqueEnemyElementSize
                    );
                    count++;
                }
            }
        }

        distanceElement = enenyElements[0].getX() - this.getX();
        distanceElementDown = enenyElements[0].getY() - this.getY();
        componentSize = enemyMatrix[0].length;
    }

    private void updateEnemyElementsLocation() {
        int distance = this.getX() + this.distanceElement - enenyElements[0].getX();
        for (EnenyElement enenyElement : enenyElements) {
            enenyElement.setX(enenyElement.getX() + distance);
        }
    }

    private void updateEnemyElementsLocationDown() {
        int distance = this.getY() + this.distanceElementDown - enenyElements[0].getY();
        for (EnenyElement enenyElement : enenyElements) {
            enenyElement.setY(enenyElement.getY() + distance);
        }
    }

    private void moveDown() {
        this.setLocation(this.getX(), this.getY() + uniqueEnemyElementSize);
        updateEnemyElementsLocationDown();
    }

    private void move() {
        if (leftDirection) {
            this.setLocation(this.getX() - uniqueEnemyElementSize, this.getY());
        } else {
            this.setLocation(this.getX() + uniqueEnemyElementSize, this.getY());
        }

        updateEnemyElementsLocation();

        if (this.getX() <= Constants.X_BEGIN) {
            this.setLeftDirection(false);
        } else if (this.getX() >= Constants.X_END - componentSize * uniqueEnemyElementSize) {
            this.setLeftDirection(true);
        }
    }

    public void draw(Graphics g) {
        for (EnenyElement enenyElement : enenyElements) {
            enenyElement.draw(g, this.getParent());
        }
    }

    public void active() {
        winStatus = false;
        populateEnemyElements(enemyInput[enemyOrder]);
        timer.start();
    }

    private void checkDestroy() {
        int count = 0;
        for (EnenyElement enenyElement : enenyElements) {
            if (enenyElement.getY() >= Constants.Y_END) {
                count++;
            }
        }

        if (count > 0) {
            EnenyElement[] newElements = new EnenyElement[enenyElements.length - count];
            int index = 0;
            for (EnenyElement enenyElement : enenyElements) {
                if (enenyElement.getY() < Constants.Y_END) {
                    newElements[index++] = enenyElement;
                }
            }
            enenyElements = newElements;

            if (enenyElements.length == 0) {
                if (enemyOrder < enemyInput.length - 1) {
                    enemyOrder++;
                    this.setLocation(Constants.X_BEGIN, Constants.Y_BEGIN - Constants.STEP_DISTANCE);
                    populateEnemyElements(enemyInput[enemyOrder]);
                } else {
                    timer.stop();
                    this.winStatus = true;
                    this.getParent().repaint();
                }
            }
        }
    }

    public void setDestroy() {
        timer.stop();
    }

    public void checkCollision(BulletComponent bulletComponent) {
        int enemyCount = enenyElements.length;
        EnenyElement[] tempElements = new EnenyElement[enemyCount];
        System.arraycopy(enenyElements, 0, tempElements, 0, enemyCount);

        int bulletCount = bulletComponent.getBulletElements().length;
        BulletElement[] tempBullets = new BulletElement[bulletCount];
        System.arraycopy(bulletComponent.getBulletElements(), 0, tempBullets, 0, bulletCount);

        int count = 0;

        for (BulletElement bulletElement : tempBullets) {
            for (EnenyElement enenyElement : tempElements) {
                if (enenyElement.getBounds().intersects(bulletElement.getBounds())) {
                    bulletComponent.removeBulletElement(bulletElement);
                    count++;
                    break; // Break to the next bullet
                }
            }
        }

        if (count > 0) {
            EnenyElement[] newElements = new EnenyElement[enenyElements.length - count];
            int index = 0;

            for (EnenyElement enenyElement : enenyElements) {
                boolean found = false;

                for (int i = 0; i < tempElements.length; i++) {
                    if (tempElements[i] == enenyElement) {
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    newElements[index++] = enenyElement;
                }
            }

            enenyElements = newElements;

            if (enenyElements.length == 0) {
                if (enemyOrder < enemyInput.length - 1) {
                    enemyOrder++;
                    this.setLocation(Constants.X_BEGIN, Constants.Y_BEGIN);
                    populateEnemyElements(enemyInput[enemyOrder]);
                } else {
                    timer.stop();
                    this.winStatus = true;
                    this.getParent().repaint();
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        move();
        if (timeToDown > 0) {
            timeToDown--;
        } else {
            this.moveDown();
            timeToDown = TIME_STEP_DOWN;
        }
        checkDestroy();
        this.getParent().repaint();
    }

    public boolean isLeftDirection() {
        return leftDirection;
    }

    public void setLeftDirection(boolean leftDirection) {
        this.leftDirection = leftDirection;
    }

    public boolean isWinStatus() {
        return winStatus;
    }

    public void setWinStatus(boolean winStatus) {
        this.winStatus = winStatus;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
