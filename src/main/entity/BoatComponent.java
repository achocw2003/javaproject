package main.entity;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

import javax.swing.Timer;

import main.constants.Constants;
import main.entity.element.BoatElement;
import main.entity.element.EnemyEnvironmentElement;
import main.entity.element.EnenyElement;
import main.enums.DirectionEnum;
import main.utils.GameKeyEventListener;

class BoatNode {
    BoatElement boatElement;
    BoatNode next;

    public BoatNode(BoatElement boatElement) {
        this.boatElement = boatElement;
        this.next = null;
    }
}

public class BoatComponent extends Component implements ActionListener {
    private static final long serialVersionUID = 1L;
    private static final int ADD_BULLET_TIME = 2;
    private BoatNode head;
    private int uniqueBoatElementSize;
    private Timer timer;
    private GameKeyEventListener keyEventListener;
    private BulletComponent bulletComponent;
    private int time = ADD_BULLET_TIME;
    private boolean gameOverStatus;

    public BoatComponent(int uniqueBoatElementSize) {
        super();
        this.uniqueBoatElementSize = uniqueBoatElementSize;
        this.initAttribute();
    }

    private void initAttribute() {
        head = null;
        bulletComponent = new BulletComponent(Constants.STEP_DISTANCE);
        this.timer = new Timer(Constants.BOAT_TIME_DELAY, this);
    }

    private void addBulletComponent() {
        bulletComponent.setBounds(this.getX(), this.getY(), uniqueBoatElementSize, uniqueBoatElementSize);
        this.getParent().add(bulletComponent);
        bulletComponent.active();
    }

    public void setKeyEventListener(GameKeyEventListener keyEventListener) {
        this.keyEventListener = keyEventListener;
    }

    public BulletComponent getBulletComponent() {
        return bulletComponent;
    }

    public void setBulletComponent(BulletComponent bulletComponent) {
        this.bulletComponent = bulletComponent;
    }

    public void initBoat() {
        addBoatElement(new BoatElement(this.getX(), this.getY(), this.uniqueBoatElementSize,
                this.uniqueBoatElementSize));
        addBoatElement(new BoatElement(this.getX() - this.uniqueBoatElementSize, this.getY(),
                this.uniqueBoatElementSize, this.uniqueBoatElementSize));
        addBoatElement(new BoatElement(this.getX() + this.uniqueBoatElementSize, this.getY(),
                this.uniqueBoatElementSize, this.uniqueBoatElementSize));
        addBoatElement(new BoatElement(this.getX(), this.getY() - this.uniqueBoatElementSize,
                this.uniqueBoatElementSize, this.uniqueBoatElementSize));
    }

    private void addBoatElement(BoatElement boatElement) {
        BoatNode newNode = new BoatNode(boatElement);
        if (head == null) {
            head = newNode;
        } else {
            BoatNode current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }
    }

    private void updateBoats() {
        int distance = this.getX() - head.boatElement.getX();
        BoatNode current = head;
        while (current != null) {
            current.boatElement.setX(current.boatElement.getX() + distance);
            current = current.next;
        }
    }

    public void active() {
        gameOverStatus = false;
        addBulletComponent();
        this.setFocusable(true);
        this.timer.start();
    }

    public void draw(Graphics g) {
        BoatNode current = head;
        while (current != null) {
            current.boatElement.draw(g, this.getParent());
            current = current.next;
        }
        bulletComponent.draw(g);
    }

    private void updateBulletLocation() {
        bulletComponent.setLocation(this.getLocation());
    }

    public void move() {
        if (Objects.equals(this.keyEventListener.getDirection(), DirectionEnum.LEFT)
                && this.getX() - uniqueBoatElementSize >= Constants.X_BEGIN) {
            this.setLocation(this.getX() - uniqueBoatElementSize, this.getY());
        } else if (Objects.equals(this.keyEventListener.getDirection(), DirectionEnum.RIGHT)
                && this.getX() + 2 * uniqueBoatElementSize <= Constants.X_END) {
            this.setLocation(this.getX() + uniqueBoatElementSize, this.getY());
        }
        updateBulletLocation();
    }

    private void addBullet() {
        bulletComponent.add();
    }

    public void destroy() {
        timer.stop();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.move();
        this.updateBoats();
        if (time > 0) {
            time--;
        } else {
            this.addBullet();
            time = ADD_BULLET_TIME;
        }

        this.getParent().repaint();
    }

    public void checkCollision(EnemyComponent enemyComponent) {
        BoatNode current = head;
        while (current != null) {
            for (EnenyElement enenyElement : enemyComponent.getEnenyElements()) {
                if (enenyElement.getBounds().intersects(current.boatElement.getBounds())) {
                    // enemyComponent.getEnenyElements().remove(enenyElement);
                    bulletComponent.destroy();
                    this.destroy();
                    this.gameOverStatus = true;
                    this.getParent().repaint();
                    return;
                }
            }
            current = current.next;
        }
    }

    public void checkCollisionFireBall(EnemyEnvironmentComponent fireBall) {
        BoatNode current = head;
        while (current != null) {
            for (EnemyEnvironmentElement enenyElement : fireBall.getEnemyEnvironmentElements()) {
                if (enenyElement.getBounds().intersects(current.boatElement.getBounds())) {
                    bulletComponent.destroy();
                    this.destroy();
                    this.gameOverStatus = true;
                    this.getParent().repaint();
                    return;
                }
            }
            current = current.next;
        }
    }

    public boolean isGameOverStatus() {
        return gameOverStatus;
    }

    public void setGameOverStatus(boolean gameOverStatus) {
        this.gameOverStatus = gameOverStatus;
    }
}
